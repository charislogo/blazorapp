﻿using BlazorAppDataLayer.Interfaces;
using BlazorAppDataLayer.Models;
using Microsoft.AspNetCore.Mvc;

namespace InternalBlazorAppApi.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet]
        public async Task<List<Customer>> Get(string? id, int? pageNumber, int? pageSize)
        {
            if (string.IsNullOrEmpty(id))
            {
                var queryParameters = Request.Query;

                if (queryParameters.Keys.Contains("pageNumber") && queryParameters.Keys.Contains("pageSize"))
                {
                    var tempSkip = (queryParameters.TryGetValue("pageNumber", out var Skip)) ? Convert.ToInt32(Skip[0]) : 0;
                    var tempTake= (queryParameters.TryGetValue("pageSize", out var Take)) ? Convert.ToInt32(Take[0]) : 5;

                    return await _customerRepository.GetAllCustomers(tempSkip, tempTake);
                }
                
                return await _customerRepository.GetAllCustomers();
            }

            var tempCustomer = await _customerRepository.GetCustomer(id);
            var listOfCustomers = new List<Customer>();
            if (tempCustomer != null)
                listOfCustomers.Add(tempCustomer);

            return listOfCustomers;
        }

        [HttpPost]
        public async void Post([FromBody] Customer customer)
        {
            _customerRepository.AddCustomer(customer);
        }

        [HttpPut]
        public async void Put([FromBody] Customer customer)
        {
            _customerRepository.UpdateCustomer(customer);
        }

        [HttpDelete]
        public async void Delete(string id)
        {
            _customerRepository.DeleteCustomer(id);
        }
    }
}
