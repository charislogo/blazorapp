﻿using BlazorAppDataLayer.Models;

namespace BlazorAppDataLayer.Interfaces
{
    public interface ICustomerRepository
    {
        public Task<List<Customer>> GetAllCustomers(int? pageNumber = null, int? pageSize = null);
        public Task<Customer> GetCustomer(string id);
        public void AddCustomer(Customer customer);
        public void UpdateCustomer(Customer customer);
        public void DeleteCustomer(string customerId);
    }
}
