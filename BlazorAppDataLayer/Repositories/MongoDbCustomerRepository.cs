﻿using System.Data;
using BlazorAppDataLayer.Interfaces;
using BlazorAppDataLayer.Models;
using BlazorAppDataLayer.Models.DatabaseSettings;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BlazorAppDataLayer.Repositories
{
    public class MongoDbCustomerRepository : ICustomerRepository
    {
        private IMongoCollection<Customer> _customers;

        public MongoDbCustomerRepository(IOptions<CustomersDatabaseSettings> dbSettings)
        {
            var customerDbSettings = dbSettings.Value;
            var client = new MongoClient(customerDbSettings.ConnectionString);
            var mongoDatabase = client.GetDatabase(customerDbSettings.DatabaseName);

            _customers = mongoDatabase.GetCollection<Customer>(customerDbSettings.CustomersCollectionName);
        }

        public async Task<List<Customer>> GetAllCustomers(int? pageNumber = null, int? pageSize = null)
        {
            try
            {
                if (pageNumber != null && pageNumber != null)
                {
                    if (pageNumber < 0)
                        throw new InvalidExpressionException($"The value of '{nameof(pageNumber)}' parameter cannot be negative.");
                    if (pageSize < 0)
                        throw new InvalidExpressionException($"The value of '{nameof(pageSize)}' parameter cannot be negative.");

                    var skip = (int)pageNumber * (int)pageSize;
                    return _customers.AsQueryable().Skip(skip).Take((int)pageSize).ToList();
                }
                return await _customers.Find(FilterDefinition<Customer>.Empty).ToListAsync() ?? new List<Customer>();
            }
            catch (Exception exception)
            {
                throw; // TODO Add exception handling
            }
        }

        public async Task<Customer> GetCustomer(string id)
        {
            try
            {
                var filter = Builders<Customer>.Filter.Eq(r => r.Id, id);
                var customer = await _customers.Find(filter).FirstOrDefaultAsync();
                return customer ?? new Customer();
            }
            catch (Exception exception)
            {
                throw; // TODO Add exception handling
            }
        }

        public async void AddCustomer(Customer customer)
        {
            try
            {
                await _customers.InsertOneAsync(customer);
            }
            catch (Exception exception)
            {
                throw; // TODO Add exception handling
            }
        }

        public async void UpdateCustomer(Customer customer)
        {
            try
            {
                var filter = Builders<Customer>.Filter.Eq(c => c.Id, customer.Id);
                await _customers.ReplaceOneAsync(filter, customer);
            }
            catch (Exception exception)
            {
                throw; // TODO Add exception handling
            }
        }

        public async void DeleteCustomer(string customerId)
        {
            try
            {
                await _customers.DeleteOneAsync(c => c.Id == customerId);
            }
            catch (Exception exception)
            {
                throw; // TODO Add exception handling
            }
        }
    }
}
