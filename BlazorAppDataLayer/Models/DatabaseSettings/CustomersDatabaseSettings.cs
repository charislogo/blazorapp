﻿namespace BlazorAppDataLayer.Models.DatabaseSettings
{
    public class CustomersDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
        public string CustomersCollectionName { get; set; } = null!;
    }
}
